`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: Josef �ada
// 
// Create Date: 27.12.2022 12:13:46
// Design Name: 
// Module Name: vertical_counter
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module vertical_counter(
    input clk_25MHz,
    input enable_V_Counter,
    output reg [15:0] V_Count_Value = 0
    );
    
        always@(posedge clk_25MHz) begin
        if ( enable_V_Counter == 1'b1) begin
            if (V_Count_Value < 525)// po��tej do 525
            V_Count_Value <= V_Count_Value +1; // u inkrementace m��e b�t probl�m <= p��padn� nahradit za =
        else V_Count_Value <= 0; // reset horizont�ln�ho ��ta�e
    end
    end  
endmodule
