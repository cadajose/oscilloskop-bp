`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: Josef �ada
// 
// Create Date: 27.12.2022 16:16:44
// Design Name: 
// Module Name: test_bench
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module test_bench;
reg clk = 0;
wire Hsynq;
wire Vsynq;
wire [3:0] Red;
wire [3:0] Green;
wire [3:0] Blue;

top UUT (clk, Hsynq, Vsynq, Red, Green, Blue);

always #5 clk = ~clk;

endmodule
