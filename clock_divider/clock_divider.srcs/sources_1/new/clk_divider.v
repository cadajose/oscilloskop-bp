`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: �VUT FEL
// Engineer: Josef �ada
// 
// Create Date: 27.12.2022 10:10:08
// Design Name: 
// Module Name: clk_divider
// Project Name: 
// Target Devices: Zybo z7 20
// Tool Versions: 
// Description: d�li�ka kmito�tu zalo�ena na ��ta�i
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module clk_divider(
input clk, // 100Mhz
output reg divided_clk = 0   //1Hz, reg je vhodn�j��

    );
    localparam div_value = 1000; // 1Hz
    integer counter_value = 0; // 32 bit� �irok� bus, mo�n� pou��t u32
    
    always@ (posedge clk) // always block se spust� kdykoliv kdy� je rising edge
    begin
    if (counter_value == div_value)
        counter_value = 0; // resetuju
    else
        counter_value <= counter_value+1; // jinak inkrementace
    end
    
    // divide clock
    always@ (posedge clk)
    begin
        if(counter_value == div_value)
            divided_clk <= ~divided_clk; // jakmile je tis�c tak p�eklop�
        
        else
            divided_clk <= divided_clk; // ulo� hodnotu
    end
endmodule
